<?php

namespace Tests\Feature;

use App\Services\ContentService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class ContentServiceTest extends TestCase
{

    public function testSaveContent()
    {
        $url = 'test url';
        $response = new Response(
            200,
            [
                'Content-Type' => 'text/html; charset=UTF-8',
            ],
            'test result'
        );

        $client = $this->createMock(Client::class);
        $client->method('__call')
            ->with('get', [$url])
            ->willReturn($response);

        $contentService = new ContentService($client);

        $result = $contentService->saveContent($url);

        $this->assertIsArray($result);

        $contentType = \GuzzleHttp\Psr7\parse_header($response->getHeader('Content-Type'))[0];

        $this->assertArraySubset([
            'url'            => $url,
            'content_length' => $response->getBody()->getSize(),
            'content_type'   => trim($contentType[0]),
            'http_status'    => $response->getStatusCode(),
        ], $result);
    }


    public function testSaveContent404()
    {
        $url = 'test url';
        $response = new Response(
            404,
            [
                'Content-Type' => 'text/html; charset=UTF-8',
            ],
            'test result'
        );

        $client = $this->createMock(Client::class);
        $client->method('__call')
            ->with('get', [$url])
            ->willThrowException(new ClientException('test', new Request('get', $url), $response));

        $contentService = new ContentService($client);

        $result = $contentService->saveContent($url);

        $this->assertIsArray($result);

        $contentType = \GuzzleHttp\Psr7\parse_header($response->getHeader('Content-Type'))[0];

        $this->assertArraySubset([
            'url'            => $url,
            'content_length' => $response->getBody()->getSize(),
            'content_type'   => trim($contentType[0]),
            'http_status'    => $response->getStatusCode(),
        ], $result);
    }


    public function testGetContent()
    {
        $url = 'https://phpunit.de';

        $client = $this->createMock(Client::class);

        $contentService = new ContentService($client);
        $result = $contentService->getContent($url);

        $this->assertTrue(true, $result);
    }

    public function testGetContentNotValidUrl()
    {
        $query = '.phpunit.de';

        $client = $this->createMock(Client::class);

        $contentService = new ContentService($client);
        $result = $contentService->getContent($query);

        $this->assertEquals(null, $result);
    }


}

