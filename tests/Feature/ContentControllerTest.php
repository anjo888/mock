<?php

namespace Tests\Feature;

use App\Interfaces\Services\ContentServiceInterface;
use App\Services\ContentService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;


class ContentControllerTest extends TestCase
{

    /**
     *
     */
    public function testValidResource()
    {
        $url = 'http://example.com';

        $response = new Response(
            200,
            [
                'Content-Type' => 'text/html; charset=UTF-8',
            ]
        );

        $client = $this->createMock(Client::class);
        $client->method('__call')
            ->with('get', [$url])
            ->willReturn($response);

        $contentService = new ContentService($client);

        $this->app->bind(ContentServiceInterface::class, function ($app) use ($contentService) {

            return $contentService;
        });


        $result = $contentService->saveContent($url);

        $this->post('/api/resource', ['url' => $url], ['Accept' => 'application/json'])->assertJson([
            'status'  => 'ok',
            'message' => 'Url has been added',
            'data'    => $result,
        ]);


    }

    public function testNotVaidUrl()
    {
        $notValidUrl = '.xample.com';
        $response = new Response(
            422,
            [
                'Content-Type' => 'text/html; charset=UTF-8',
            ]
        );

        $client = $this->createMock(Client::class);
        $client->method('__call')
            ->with('get', [$notValidUrl])
            ->willReturn($response);

        $contentService = new ContentService($client);

        $this->app->bind(ContentServiceInterface::class, function ($app) use ($contentService) {

            return $contentService;
        });

        $this->post('/api/resource', ['url' => $notValidUrl], ['Accept' => 'application/json'])->assertJson([
            'status'  => 'error',
            'message' => 'Validation failed',
            'errors'  => ['url' => [0 => 'The url format is invalid.']],
            'data'    => ['url' => $notValidUrl],
        ]);
    }



}
