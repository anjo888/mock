<?php

namespace App\Services;

use App\Interfaces\Services\ContentServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ContentService implements ContentServiceInterface
{
    private $client;

    /**
     * ContentServiceInterface constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $url
     *
     * @return mixed
     */
    public function getContent($url): ?string
    {

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return null;
        }
        $queryEncode = urlencode($url);

        if ($result = $this->loadFromCache($queryEncode)) {

            return $result['content'];
        }
        return 'нет контента';
    }

    /**
     * @param $url
     *
     * @return array|null
     */
    public function saveContent($url): ?array
    {

        try {
            $response = $this->client->get($url);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        } catch (\Throwable $e) {
            Log::error($e->getMessage(), ['url' => $url]);

            return null;
        }

        $content = [];

        try {
            $type = $response->getHeaderLine('content-type');
            $parsed = \GuzzleHttp\Psr7\parse_header($type);
            if (isset($parsed[0]['charset'])) {
                $content['content'] = mb_convert_encoding(
                    $response->getBody()->getContents(),
                    'UTF-8',
                    $parsed[0]['charset'] ?: 'UTF-8'
                );
            } else {
                $content['content'] = $response->getBody()->getContents();
            }
        } catch (\Throwable $e) {
            Log::error($e->getMessage(), ['content_type' => $parsed[0][0]]);
        }


        $content['id'] = md5($url);
        $meta = [
            'url'            => $url,
            'content_id'     => $content['id'],
            'content_length' => mb_strlen($content['content']),
            'content_type'   => $parsed[0][0],
            'http_status'    => $response->getStatusCode(),
        ];

        $encodedUrl = urlencode($url);
        $this->saveToCache($encodedUrl, $content);
        $this->saveToMeta($encodedUrl, $meta);

        if (Cache::has(md5($encodedUrl))) {
            return $meta;
        }

        return null;
    }

    /**
     * @param $query
     *
     * @return mixed
     */

    public function getMeta($query)
    {
        $query = urlencode($query);
        if (Cache::has($query)) {
            return true;
        }
        return Cache::store('redis')->get($query);
    }

    /**
     * @param $query
     * @param $meta
     */
    private function saveToMeta($query, $meta): void
    {
        Cache::store('redis')->put($query, $meta, 2000);
    }

    /**
     * @param $query
     * @param $content
     */
    private function saveToCache($query, $content): void
    {
        Cache::forever(md5($query), $content);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    private function loadFromCache($query)
    {
        return Cache::get(md5($query));
    }

}