<?php

namespace App\Interfaces\Services;

interface ContentServiceInterface
{
    /**
     * @param $url
     *
     * @return mixed
     */
    public function getContent($url): ?string;

    /**
     * @param $url
     * @return array|null
     */
    public function saveContent($url): ?array;

    /**
     * @param $query
     * @return mixed
     */
    public function getMeta($query);
}