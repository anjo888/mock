<?php

namespace App\Providers;

use App\Interfaces\Services\ContentServiceInterface;
use App\Services\ContentService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ContentServiceInterface::class, ContentService::class);
    }
}
