<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddResourceRequest;
use App\Interfaces\Services\ContentServiceInterface;


/**
 * Class ContentController
 *
 * @package App\Http\Controllers
 */
class ContentController
{
    public $service;

    /**
     * ContentController constructor.
     *
     * @param ContentServiceInterface $service
     */
    public function __construct(ContentServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param AddResourceRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function addResource(AddResourceRequest $request)
    {

        $url = $request->post('url');
        $result = $this->service->saveContent($url);

        if ($result !== null) {

            return response()->json(['status' => 'ok', 'message' => 'Url has been added', 'data' => $result]);
        }

        return response()->json(['status' => 'error', 'message' => 'Error', 'data' => ['url' => $url]]);
    }


}
