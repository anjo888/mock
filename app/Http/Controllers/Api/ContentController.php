<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\GetMetaRequest;
use App\Interfaces\Services\ContentServiceInterface;
use Illuminate\Http\Request;

class ContentController
{
    public $service;

    public function __construct(ContentServiceInterface $service)
    {
        $this->service = $service;

    }

    /**
     * @param Request $request
     *
     * @return mixed|string
     */
    public function getContent(Request $request)
    {
        $query = $request->input('q');

        return $this->service->getContent($query) ?? view('nocontent');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function getMeta(GetMetaRequest $request)
    {
        $meta = $request->get('meta');
        $result = $this->service->getMeta($meta);
        if ($result !== null) {
            return response()->json(['status' => 'ok', 'message' => 'Metadata has been added', 'data' => $result]);
        }

        return response()->json(['status' => 'error', 'message' => 'No metadata', 'data' => ['url' => $meta]]);
    }

}