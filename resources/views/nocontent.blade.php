<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <title>Просмотр контента</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>Просмотр контента</h2>
            <form class="form-inline" action="{{route('getContent')}}" method="get" enctype="multipart/form-data">
                <div class="form-group">
                    <input class="form-control" type="url" name="q" placeholder="Url" required>
                    <button type="submit" class="btn btn-primary">Просмотр</button>
                </div>

            </form>
        </div>
    </div>
</div>


</body>
</html>


