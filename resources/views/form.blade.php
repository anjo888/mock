<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <title>Добавление контента</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>Добавление контента</h2>
            <form class="form-inline" action="{{route('saveContent')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="url" class="form-control" name="url" placeholder="Url" required>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>

            </form>

        </div>
    </div>
</div>

</body>
</html>


